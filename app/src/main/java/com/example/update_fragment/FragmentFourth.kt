package com.example.update_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_first.*
import kotlinx.android.synthetic.main.fragment_second.*

class FragmentFourth:Fragment() , View.OnClickListener {
//    var isFirstButtonClicked = true
//    val fragment = MainActivity()

    lateinit var btn: Button
    lateinit var tv: TextView
    lateinit var comm: Communicator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_fourth, container, false)

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        comm = activity as Communicator
        btn = activity?.findViewById(R.id.btn4) as Button
        tv = activity?.findViewById(R.id.tv4) as TextView

        btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        comm.respond4("Fragment Four Button Clicked")
    }

    fun changeText(data: String){
        tv.setText(data)
    }
}


//        btn_Fragment_One.setOnClickListener{
//            textView.text ="1"


//    btn.setOnClickListener{
//
//        val input = editText.text.toString()
//
//        val bundle = Bundle()
//        bundle.putString("data",input)

//        val fragment = FragmentSecond()
//        fragment.arguments = bundle
//        fragmentManager?.beginTransaction()?.replace(R.id.)
//    }
//    return view
//}

//    view.btn_Fragment_One.setOnClickListener{
//        if (isFirstButtonClicked){
//
//        }
//        fragment.update(view.etFragmentOne.text.toString())
//    }
//
//    return view
//    }


//}