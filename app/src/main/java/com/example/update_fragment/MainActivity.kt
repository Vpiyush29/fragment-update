package com.example.update_fragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.FragmentManager

class MainActivity : AppCompatActivity() , Communicator {


//    lateinit var tv: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        tv = findViewById(R.id.tv) as TextView

//        supportFragmentManager.beginTransaction().replace(R.id.container_1, fragmentone )
//            .replace(R.id.container_2, fragmenttwo )
//            .replace(R.id.container_3,fragmentthree)
//            .replace(R.id.container_4,fragmentfour)
//            .commit()
//
//        tv = findViewById(R.id.etFragmentOne) as TextView
//

    }

    override fun respond1(data: String) {
        var manager: FragmentManager = supportFragmentManager
        var f2 : FragmentSecond = manager.findFragmentById(R.id.f2) as FragmentSecond
        f2.changeText(data)
//        tv.setText(data)
    }

    override fun respond2(data: String) {
        var manager: FragmentManager = supportFragmentManager
        var f3 : FragmentThird = manager.findFragmentById(R.id.f3) as FragmentThird
        f3.changeText(data)
//        tv.setText(data)
    }

    override fun respond3(data: String) {
        var manager: FragmentManager = supportFragmentManager
        var f4 : FragmentFourth = manager.findFragmentById(R.id.f4) as FragmentFourth
        f4.changeText(data)
//        tv.setText(data)
    }

    override fun respond4(data: String) {
        var manager: FragmentManager = supportFragmentManager
        var f1 : FragmentFirst = manager.findFragmentById(R.id.f1) as FragmentFirst
        f1.changeText(data)
//        tv.setText(data)
    }

}