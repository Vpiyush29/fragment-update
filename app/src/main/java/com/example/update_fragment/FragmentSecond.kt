package com.example.update_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment

class FragmentSecond: Fragment() , View.OnClickListener  {


    lateinit var btn: Button
    lateinit var tv: TextView
    lateinit var comm: Communicator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_second, container, false)

            return view
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            super.onActivityCreated(savedInstanceState)
            comm = activity as Communicator
            btn = activity?.findViewById(R.id.btn2) as Button
            tv = activity?.findViewById(R.id.tv2) as TextView
            btn.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            comm.respond2("Fragment Two Button Clicked")
        }

        fun changeText(data: String) {
            tv.setText(data)
        }
    }